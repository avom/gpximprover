# GPX Improver #

This project is developed as part of an [Algorithmics course](https://courses.cs.ut.ee/2014/algorithmics/fall/Main/HomePage) in University of Tartu.

It is a proof of concept and far from being fully featured production level tool.

Source code open source licensed under **MIT license** (except some external library source under this repository, see below).

## What it is and who it's for? ##

Target audience for GpxImprover are endurance athletes.

It is a utility to fix issues in a recorded GPS data and it accepts GPX files:

1. If GPS device (i. e. a cell phone) has recorded several consecutive data points with same coordinates, then it tries to guess if athlete was actually moving. If so, then it will spread those points so the route won't change, but GPS data will indicate steady pace during those points after the utility has done it's job.
2. If GPS device hasn't recorded anything for awhile, then it creates new data points during that time and spreads them as described in previous point.
3. Given a directory containing GPX files (training data), the utility tries to modify coordinates in recorded GPX file to make the track look more similar to tracks in training data. This is good when your device lost GPS signal during training/race etc. Naturally all tracks (both input file and training data) must travel the same route for this to work.

tl;dr...it will fix your GPX file based on other GPX files that are recorded on exactly the same track.

## How to use it? ##

You need to compile it frist. I used Xamarin Studio for developing. You also have to compile Geo library as well and manually (sorry!) copy it's dll over the one in GpxImprover packages directory.

The application requires two command line arguments:
1. File name of the GPX file you are trying to fix.
2. Directory of GPX files that contain same track (for example your earlier runs through same route).

Example for Windows:
GpxImprover.exe todays_10k_lap.gpx folder_of_my_previous_10k_laps

NB! The utility expects .Net 4.5 support (or Mono).

## How it works? ##

See A1 [poster] (https://bitbucket.org/avom/gpximprover/src/master/docs/poster.pdf).

## Third party libraries ##

1. [Geo - a geospatial library for .NET](https://github.com/sibartlett/Geo). I have explicitly imported source of this library under this repository because some changes to it's source were required. It is under a different license (GNU LGPL) compared to GpxImprover project. See Geo project page for details.
2. [Castle Windsor](http://docs.castleproject.org/Windsor.MainPage.ashx)
3. [Rhino Mocks](http://hibernatingrhinos.com/oss/rhino-mocks)
4. [NUnit](http://www.nunit.org/)