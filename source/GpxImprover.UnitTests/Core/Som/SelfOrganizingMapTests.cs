﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Geodesy;
using Geo.Gps;
using GpxImprover.Core.Common;
using GpxImprover.Core.Som;
using NUnit.Framework;
using Rhino.Mocks;

namespace GpxImprover.UnitTests.Core.Som
{
    [TestFixture]
    public class SelfOrganizingMapTests
    {
        DistanceCalculator calculator = new DistanceCalculator(new SpheroidCalculator());

        [Test]
        public void Learn_LambdaIs1_MovesAllPointsTowardTrainingData()
        {
            const int lambda = 1;
            var neighborhoodFunc = MockRepository.GenerateStrictMock<INeighborhoodFunction>();
            var sut = new SelfOrganizingMap(calculator, lambda, neighborhoodFunc);

            neighborhoodFunc.Expect(x => x.GetDegree(0)).Return(1);
            neighborhoodFunc.Expect(x => x.GetDegree(Arg<int>.Is.Anything)).Return(0);

            var training = new List<Coordinate>();
            training.Add(new Coordinate(0, 0));
            training.Add(new Coordinate(1, 1));
            training.Add(new Coordinate(2, 2));
            sut.AddTrainingData(training);

            var fixes = new List<Fix>();
            fixes.Add(new Fix(0.0, 0.0, DateTime.MinValue.AddSeconds(1)));
            fixes.Add(new Fix(0.9, 1.1, DateTime.MinValue.AddSeconds(2)));
            fixes.Add(new Fix(2.1, 1.9, DateTime.MinValue.AddSeconds(3)));
            sut.Learn(fixes);

            Assert.IsTrue(isBetween(fixes[0].Coordinate.Latitude, 0.0, 0.0), "fixes[0].Latitude");
            Assert.IsTrue(isBetween(fixes[0].Coordinate.Longitude, 0.0, 0.0), "fixes[0].Longitude");
            Assert.IsTrue(isBetween(fixes[1].Coordinate.Latitude, 0.9, 1.0), "fixes[1].Latitude");
            Assert.IsTrue(isBetween(fixes[1].Coordinate.Longitude, 1.0, 1.1), "fixes[1].Longitude");
            Assert.IsTrue(isBetween(fixes[2].Coordinate.Latitude, 2.0, 2.1), "fixes[2].Latitude");
            Assert.IsTrue(isBetween(fixes[2].Coordinate.Longitude, 1.9, 2.0), "fixes[2].Longitude");
        }

        bool isBetween(double x, double min, double max)
        {
            return (x >= min) && (x <= max);
        }
    }
}

