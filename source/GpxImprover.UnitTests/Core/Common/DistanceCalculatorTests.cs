﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Abstractions.Interfaces;
using Geo.Geodesy;
using Geo.Measure;
using GpxImprover.Core.Common;
using NUnit.Framework;
using Rhino.Mocks;

namespace GpxImprover.UnitTests.Core.Common
{
    [TestFixture]
    public class DistanceCalculatorTests
    {
        [Test]
        public void GetDistance_BetweenSamePoint_Returns0()
        {
            var calculator = new SpheroidCalculator();
            var sut = new DistanceCalculator(calculator);

            var returnValue = sut.GetDistance(new Coordinate(10, 20), new Coordinate(10, 20));

            Assert.AreEqual(0.0, returnValue.SiValue, 0.001, "Invalid distance");
        }

        [Test]
        public void GetDistance_BetweenTwoPoints_ReturnsDistance()
        {
            var line = new GeodeticLine(new Coordinate(0, 0), new Coordinate(1, 1), 10, 0, 0);

            var calculatorStub = MockRepository.GenerateStub<IGeodeticCalculator>();
            calculatorStub.Expect(x => x.CalculateOrthodromicLine(null, null))
                .IgnoreArguments()
                .Return(line);

            var sut = new DistanceCalculator(calculatorStub);

            var returnValue = sut.GetDistance(line.Coordinate1, line.Coordinate2);

            Assert.AreEqual(10.0, returnValue.SiValue, 0.001, "Invalid distance");
        }

        [Test]
        public void GetDistance_ListOfPoints_ReturnsTotalDistance()
        {
            var line1 = new GeodeticLine(new Coordinate(0, 0), new Coordinate(1, 1), 10, 0, 0);
            var line2 = new GeodeticLine(line1.Coordinate2, new Coordinate(3, 3), 30, 0, 0);

            var calculatorStub = MockRepository.GenerateStub<IGeodeticCalculator>();
            calculatorStub.Expect(x => x.CalculateOrthodromicLine(line1.Coordinate1, line1.Coordinate2))
                .Return(line1);
            calculatorStub.Expect(x => x.CalculateOrthodromicLine(line2.Coordinate1, line2.Coordinate2))
                .Return(line2);

            var sut = new DistanceCalculator(calculatorStub);
            var coords = new List<Coordinate>(new Coordinate[3]
                { 
                    line1.Coordinate1, line1.Coordinate2, line2.Coordinate2
                });
            var returnValue = sut.GetDistance(coords);

            Assert.AreEqual(40.0, returnValue.SiValue, 0.001, "Invalid distance");
        }
    }
}

