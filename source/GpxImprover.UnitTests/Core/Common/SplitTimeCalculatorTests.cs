﻿using System;
using System.Collections.Generic;
using Geo.Gps;
using Geo.Measure;
using GpxImprover.Core.Common;
using NUnit.Framework;
using Rhino.Mocks;

namespace GpxImprover.UnitTests.Core.Common
{
    [TestFixture]
    public class SplitTimeCalculatorTests
    {
        readonly DateTime exerciseStartTime = new DateTime(2000, 1, 1, 0, 0, 0);

        [Test]
        public void GetSplitTimes_NoFixes_Returns0()
        {
            var distanceCalculatorMock = MockRepository.GenerateStrictMock<IDistanceCalculator>();
            var sut = new SplitTimeCalculator(distanceCalculatorMock);
            var returnValue = sut.GetSplitTimes(new List<Fix>(), 100);
            Assert.AreEqual(0, returnValue.Count, "Invalid number of splits");
        }

        [Test]
        public void GetSplitTimes_TwoFixesExactlySplitDistance_ReturnsTotalTime()
        {
            var distanceCalculatorStub = MockRepository.GenerateStub<IDistanceCalculator>();
            distanceCalculatorStub.Expect(x => x.GetDistance(null, null))
                .IgnoreArguments()
                .Return(new Distance(100));

            var fixes = new List<Fix>();
            fixes.Add(new Fix(0, 0, exerciseStartTime));
            fixes.Add(new Fix(1, 1, exerciseStartTime.AddSeconds(20)));

            var sut = new SplitTimeCalculator(distanceCalculatorStub);
            var returnValue = sut.GetSplitTimes(fixes, 100);

            Assert.AreEqual(1, returnValue.Count, "Invalid number of splits");
            Assert.AreEqual(20, returnValue[0].TotalSeconds, 0.001, "Invalid split time");
        }

        [Test]
        public void GetSplitTimes_FourFixesAndSecondSplitStartsBetweenTwoFixes_ReturnsTwoSplits()
        {
            var distanceCalculatorStub = MockRepository.GenerateStub<IDistanceCalculator>();
            distanceCalculatorStub.Expect(x => x.GetDistance(null, null))
                .IgnoreArguments()
                .Return(new Distance(80));

            var fixes = new List<Fix>();
            fixes.Add(new Fix(0, 0, exerciseStartTime));
            fixes.Add(new Fix(1, 1, exerciseStartTime.AddSeconds(20)));
            fixes.Add(new Fix(2, 2, exerciseStartTime.AddSeconds(50)));
            fixes.Add(new Fix(3, 3, exerciseStartTime.AddSeconds(75)));

            var sut = new SplitTimeCalculator(distanceCalculatorStub);
            var returnValue = sut.GetSplitTimes(fixes, 100);

            Assert.AreEqual(2, returnValue.Count, "Invalid number of splits");
            Assert.AreEqual(27.5, returnValue[0].TotalSeconds, 0.001, "Invalid first split time");
            Assert.AreEqual(35.0, returnValue[1].TotalSeconds, 0.001, "Invalid second split time");
        }
    }
}

