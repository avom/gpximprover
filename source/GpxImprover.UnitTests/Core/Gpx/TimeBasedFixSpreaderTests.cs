﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Gps;
using GpxImprover.Core.Gpx;
using NUnit.Framework;

namespace GpxImprover.UnitTests.Core.Gpx
{
    [TestFixture]
    public class TimeBasedFixSpreaderTests
    {
        readonly DateTime exerciseStart = new DateTime(2000, 1, 1, 0, 0, 0);

        [Test]
        public void Spread()
        {
            var sut = new TimeBasedFixSpreader();
            var fixes = new List<Fix>();
            fixes.Add(new Fix(0, 0, getSeconds(0)));
            fixes.Add(new Fix(1, 10, getSeconds(1)));
            fixes.Add(new Fix(1, 10, getSeconds(2)));
            fixes.Add(new Fix(1, 10, getSeconds(6)));
            fixes.Add(new Fix(1, 10, getSeconds(8)));
            fixes.Add(new Fix(3, 11, getSeconds(11)));
            fixes.Add(new Fix(4, 12, getSeconds(20)));

            sut.Spread(fixes, 1, fixes.Count - 2);

            Assert.AreEqual(new Coordinate(0, 0), fixes[0].Coordinate, "fixes[0]");
            Assert.AreEqual(new Coordinate(1, 10), fixes[1].Coordinate, "fixes[1]");
            Assert.AreEqual(new Coordinate(1.2, 10.1), fixes[2].Coordinate, "fixes[2]");
            Assert.AreEqual(new Coordinate(2, 10.5), fixes[3].Coordinate, "fixes[3]");
            Assert.AreEqual(new Coordinate(2.4, 10.7), fixes[4].Coordinate, "fixes[4]");
            Assert.AreEqual(new Coordinate(3, 11), fixes[5].Coordinate, "fixes[5]");
            Assert.AreEqual(new Coordinate(4, 12), fixes[6].Coordinate, "fixes[6]");
        }

        DateTime getSeconds(int seconds)
        {
            return exerciseStart.AddSeconds(seconds);
        }
    }
}

