﻿using System;
using System.Collections.Generic;
using Geo.Gps;
using GpxImprover.Core.Gpx;
using NUnit.Framework;
using Rhino.Mocks;

namespace GpxImprover.UnitTests.Core.Gpx
{
    [TestFixture]
    public class MissingFixesRestorerTests
    {
        readonly DateTime exerciseStart = new DateTime(2000, 1, 1, 0, 0, 0);

        [Test]
        public void Restore_NoLongTimesBetweenFixes_DoesNothing()
        {
            var fixSpreaderMock = MockRepository.GenerateStrictMock<IFixSpreader>();

            var sut = new MissingFixesRestorer(fixSpreaderMock, 3);

            var fixes = new List<Fix>();
            fixes.Add(new Fix(0, 0, exerciseStart));
            fixes.Add(new Fix(1, 2, exerciseStart.AddSeconds(1)));
            fixes.Add(new Fix(2, 4, exerciseStart.AddSeconds(3)));
            fixes.Add(new Fix(4, 5, exerciseStart.AddSeconds(6)));

            sut.Restore(fixes);

            Assert.AreEqual(4, fixes.Count, "Number of fixes should not change");
        }

        [Test]
        public void Restore_TimeBetweenFixesOneSecondToLong_InsertOneFix()
        {
            var fixSpreaderStub = MockRepository.GenerateStub<IFixSpreader>();
            fixSpreaderStub.Expect(x => x.Spread(null, 0, 0)).IgnoreArguments();

            var sut = new MissingFixesRestorer(fixSpreaderStub, 4);

            var fixes = new List<Fix>();
            fixes.Add(new Fix(1, 1, exerciseStart));
            fixes.Add(new Fix(5, 9, exerciseStart.AddSeconds(5)));

            sut.Restore(fixes);

            // note that our fixSpreaderStub doesn't fix fixes[1].Coordinate
            Assert.AreEqual(3, fixes.Count, "Invalid number of fixes");
            Assert.AreEqual(new Fix(1, 1, exerciseStart), fixes[0], "fixes[0]");
            Assert.AreEqual(new Fix(1, 1, exerciseStart.AddSeconds(4)), fixes[1], "fixes[1]");
            Assert.AreEqual(new Fix(5, 9, exerciseStart.AddSeconds(5)), fixes[2], "fixes[2]");
        }
    }
}

