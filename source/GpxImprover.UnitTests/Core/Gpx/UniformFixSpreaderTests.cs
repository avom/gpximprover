﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Gps;
using GpxImprover.Core.Gpx;
using NUnit.Framework;

namespace GpxImprover.UnitTests.Core.Gpx
{
    [TestFixture]
    public class UniformFixSpreaderTests
    {
        [Test]
        public void Spread()
        {
            var sut = new UniformFixSpreader();
            var fixes = new List<Fix>();
            fixes.Add(new Fix(1, 2, DateTime.MinValue));
            fixes.Add(new Fix(2, 3, DateTime.MinValue));
            fixes.Add(new Fix(2, 3, DateTime.MinValue));
            fixes.Add(new Fix(2, 3, DateTime.MinValue));
            fixes.Add(new Fix(2, 3, DateTime.MinValue));
            fixes.Add(new Fix(4, 3.6, DateTime.MinValue));
            fixes.Add(new Fix(5, 4, DateTime.MinValue));

            sut.Spread(fixes, 1, fixes.Count - 2);

            Assert.AreEqual(new Coordinate(1, 2), fixes[0].Coordinate, "fixes[0]");
            Assert.AreEqual(new Coordinate(2, 3), fixes[1].Coordinate, "fixes[1]");
            Assert.AreEqual(new Coordinate(2.5, 3.15), fixes[2].Coordinate, "fixes[2]");
            Assert.AreEqual(new Coordinate(3, 3.3), fixes[3].Coordinate, "fixes[3]");
            Assert.AreEqual(new Coordinate(3.5, 3.45), fixes[4].Coordinate, "fixes[4]");
            Assert.AreEqual(new Coordinate(4, 3.6), fixes[5].Coordinate, "fixes[5]");
            Assert.AreEqual(new Coordinate(5, 4), fixes[6].Coordinate, "fixes[6]");
        }
    }
}

