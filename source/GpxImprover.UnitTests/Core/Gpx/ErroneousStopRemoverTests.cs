﻿using System;
using System.Collections.Generic;
using Geo.Gps;
using GpxImprover.Core.Gpx;
using NUnit.Framework;
using Rhino.Mocks;
using Geo.Geodesy;
using GpxImprover.Core.Common;

namespace GpxImprover.UnitTests.Core.Gpx
{
    [TestFixture]
    public class ErroneousStopRemoverTests
    {
        readonly DistanceCalculator distanceCalculator = 
            new DistanceCalculator(new SpheroidCalculator());
        readonly DateTime exerciseStart = new DateTime(2000, 1, 1, 0, 0, 0);
        List<Fix> fixes;

        [SetUp]
        public void Setup()
        {
            fixes = new List<Fix>();
        }

        [Test]
        public void SpreadErroneousStops_EmptySegment_NothingChanged()
        {
            var fixSpreaderMock = MockRepository.GenerateStrictMock<IFixSpreader>();
            var sut = new ErroneousStopRemover(fixSpreaderMock, distanceCalculator);
            sut.SpreadErroneousStops(fixes);
            fixSpreaderMock.VerifyAllExpectations();
        }

        [Test]
        public void SpreadErroneousStops_AllCoordinatesTheSame_NothingChanged()
        {
            var fixSpreaderMock = MockRepository.GenerateStrictMock<IFixSpreader>();
            var sut = new ErroneousStopRemover(fixSpreaderMock, distanceCalculator);
            addFix(1, 1);
            addFix(1, 1);
            addFix(1, 1);
            sut.SpreadErroneousStops(fixes);
            fixSpreaderMock.VerifyAllExpectations();
        }

        [Test]
        public void SpreadErroneousStops_StopInTheBeginning_RemoveStop()
        {
            var fixSpreaderMock = MockRepository.GenerateStrictMock<IFixSpreader>();
            var sut = new ErroneousStopRemover(fixSpreaderMock, distanceCalculator);
            addFix(1, 1);
            addFix(1, 1);
            addFix(3, 3);
            fixSpreaderMock.Expect(x => x.Spread(fixes, 0, 2));
            sut.SpreadErroneousStops(fixes);
            fixSpreaderMock.VerifyAllExpectations();
        }

        [Test]
        public void SpreadErroneousStops_TwoStops_RemoveStops()
        {
            var fixSpreaderMock = MockRepository.GenerateStrictMock<IFixSpreader>();
            var sut = new ErroneousStopRemover(fixSpreaderMock, distanceCalculator);

            addFix(0, 0);
            addFix(0.5, 0.5);
            addFix(1, 1);
            addFix(1, 1);
            addFix(2, 2);
            addFix(2.5, 2.5);
            addFix(2.5, 2.5);
            addFix(2.5, 2.5);
            addFix(2.5, 2.5);
            addFix(4.5, 4.5);
            addFix(5, 5);

            fixSpreaderMock.Expect(x => x.Spread(fixes, 2, 4));
            fixSpreaderMock.Expect(x => x.Spread(fixes, 5, 9));

            sut.SpreadErroneousStops(fixes);

            fixSpreaderMock.VerifyAllExpectations();
        }

        [Test]
        public void SpreadErroneousStops_ActualStop_NothingChanged()
        {
            var fixSpreaderMock = MockRepository.GenerateStrictMock<IFixSpreader>();
            var sut = new ErroneousStopRemover(fixSpreaderMock, distanceCalculator);

            const double meterInDegreesOnEquator = 360 / 40000 / 1000;

            addFix(-1.0, -1.0);
            addFix(-0.5, -0.5);
            addFix(0, 0);
            addFix(0, 0);
            addFix(0, 0);
            addFix(meterInDegreesOnEquator, 0);

            sut.SpreadErroneousStops(fixes);

            fixSpreaderMock.VerifyAllExpectations();
        }

        void addFix(double latitude, double longitude)
        {
            addFix(latitude, longitude, fixes.Count);
        }

        void addFix(double latitude, double longitude, int seconds)
        {
            fixes.Add(new Fix(latitude, longitude, getDuration(seconds)));
        }

        DateTime getDuration(int seconds)
        {
            return exerciseStart.AddSeconds(seconds);
        }
    }
}

