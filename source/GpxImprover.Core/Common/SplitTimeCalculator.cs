﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Gps;

namespace GpxImprover.Core.Common
{
    public class SplitTimeCalculator
    {
        readonly IDistanceCalculator calculator;

        public SplitTimeCalculator(IDistanceCalculator calculator)
        {
            if (calculator == null)
                throw new ArgumentNullException("calculator");
            this.calculator = calculator;
        }

        public List<TimeSpan> GetSplitTimes(List<Fix> fixes, double splitMeters)
        {
            if (fixes == null)
                throw new ArgumentNullException("fixes");
            if (splitMeters < 1)
            {
                throw new ArgumentOutOfRangeException("splitMeters", 
                    "split length should be at least 1 meter");
            }

            var result = new List<TimeSpan>();
            var currentSplitTime = new TimeSpan();
            double currentSplitDistance = 0.0;
            for (int i = 1; i < fixes.Count; i++)
            {
                var fromCoord = fixes[i - 1].Coordinate;
                var toCoord = fixes[i].Coordinate;
                double distance = calculator.GetDistance(fromCoord, toCoord).SiValue;
                TimeSpan time = fixes[i].TimeUtc - fixes[i - 1].TimeUtc;

                currentSplitDistance += distance;
                if (currentSplitDistance < splitMeters)
                    currentSplitTime += time;
                else
                {
                    double distanceToSplitEnd = distance - (currentSplitDistance - splitMeters);
                    long ticksToSplitEnd = 
                        (long)Math.Round((distanceToSplitEnd / distance) * time.Ticks);
                    var splitTime = new TimeSpan(currentSplitTime.Ticks + ticksToSplitEnd);
                    result.Add(splitTime);
                    currentSplitTime += time - splitTime;
                    currentSplitDistance -= splitMeters;
                }
            }
            return result;
        }
    }
}

