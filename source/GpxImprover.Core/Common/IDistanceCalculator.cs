﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Measure;

namespace GpxImprover.Core.Common
{
    public interface IDistanceCalculator
    {
        Distance GetDistance(Coordinate coord1, Coordinate coord2);

        Distance GetDistance(List<Coordinate> coordinates);
    }
}

