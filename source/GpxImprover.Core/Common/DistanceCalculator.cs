﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Abstractions.Interfaces;
using Geo.Geodesy;
using Geo.Measure;

namespace GpxImprover.Core.Common
{
    public class DistanceCalculator : IDistanceCalculator
    {
        readonly IGeodeticCalculator calculator;

        public DistanceCalculator(IGeodeticCalculator calculator)
        {
            if (calculator == null)
                throw new ArgumentNullException("calculator");

            this.calculator = calculator;
        }

        public Distance GetDistance(Coordinate coord1, Coordinate coord2)
        {
            if (coord1 == null)
                throw new ArgumentNullException("coord1");
            if (coord2 == null)
                throw new ArgumentNullException("coord2");

            GeodeticLine line = calculator.CalculateOrthodromicLine(coord1, coord2);
            return line != null ? line.Distance : new Distance(0);
        }

        public Distance GetDistance(List<Coordinate> coordinates)
        {
            if (coordinates == null)
                throw new ArgumentNullException("coordinates");

            var result = new Distance();
            for (int i = 1; i < coordinates.Count; i++)
                result += GetDistance(coordinates[i - 1], coordinates[i]);
            return result;
        }
    }
}

