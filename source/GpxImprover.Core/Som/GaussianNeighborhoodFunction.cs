﻿using System;

namespace GpxImprover.Core.Som
{
    public class GaussianNeighborhoodFunction : INeighborhoodFunction
    {
        public double GetDegree(int distanceFromWinner)
        {
            return gaussian(distanceFromWinner);
        }

        double gaussian(int x)
        {
            const double a = 1;
            const double b = 0;
            const double c = 1;
            return a * Math.Exp(-(x - b) * (x - b) / (2 * c * c));
        }
    }
}

