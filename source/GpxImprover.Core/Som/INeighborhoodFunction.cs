﻿namespace GpxImprover.Core.Som
{
    public interface INeighborhoodFunction
    {
        double GetDegree(int distanceFromWinner);
    }
}
