﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Gps;
using Geo.Measure;
using GpxImprover.Core.Common;

namespace GpxImprover.Core.Som
{
    public class SelfOrganizingMap
    {
        readonly uint lambda;
        readonly DistanceCalculator distanceCalculator;
        readonly INeighborhoodFunction neighborhood;

        readonly List<List<Coordinate>> trainingData = new List<List<Coordinate>>();

        List<Neuron> map;
        uint s;
        int closestIndex;

        public SelfOrganizingMap(DistanceCalculator distanceCalculator, uint lambda,
                                 INeighborhoodFunction neighborhood)
        {
            if (distanceCalculator == null)
                throw new ArgumentNullException("distanceCalculator");
            if (neighborhood == null)
                throw new ArgumentNullException("neighborhood");

            this.distanceCalculator = distanceCalculator;
            this.lambda = lambda;
            this.neighborhood = neighborhood;
        }

        public void AddTrainingData(List<Coordinate> coordinates)
        {
            if (coordinates == null)
                throw new ArgumentNullException("coordinates");

            trainingData.Add(coordinates);
        }

        public void Learn(List<Fix> fixes)
        {
            if (fixes == null)
                throw new ArgumentNullException("fixes");

            this.map = fixes.ConvertAll(fix => new Neuron(fix));
            if (trainingData.Count == 0)
                return;

            var rng = new Random();
            int t = 0;
            for (s = 0; s < lambda; s++)
            {
                var D = trainingData[t];
                closestIndex = 0;
                for (int i = 0; i < map.Count; i++)
                {
                    if ((closestIndex < D.Count - 1) && (rng.NextDouble() > 0.5))
                        closestIndex++;
                    closestIndex = findClosestIndex(map[i], D, closestIndex);
                    updateWeightVectors(i, D[closestIndex]);
                }
                t = (t + 1) % trainingData.Count;
            }
        }

        public void Map()
        {
            this.map.ForEach(neuron => neuron.Map());
        }

        int findClosestIndex(Neuron neuron, List<Coordinate> D, int startIndex)
        {
            int closestIndex = startIndex;
            var closestDistance = 
                distanceCalculator.GetDistance(D[closestIndex], neuron.Coordinate);
            var lastIndex = Math.Min(closestIndex + 20, D.Count) - 1;
            for (int i = startIndex + 1; i <= lastIndex; i++)
            {
                var distance = distanceCalculator.GetDistance(D[i], neuron.Coordinate);
                if (distance < closestDistance)
                {
                    closestIndex = i;
                    closestDistance = distance;
                }
            }
            return closestIndex;
        }

        void updateWeightVectors(int winner, Coordinate coordinate)
        {
            var degree = 1.0;
            map[winner].UpdateWeight(learningRate, degree, coordinate);
            int distanceFromWinner = 1;
            while (degree > 0.001)
            {
                degree = neighborhood.GetDegree(distanceFromWinner);
                if (winner >= distanceFromWinner)
                    map[winner - distanceFromWinner].UpdateWeight(learningRate, degree, coordinate);
                if (winner + distanceFromWinner < map.Count)
                    map[winner + distanceFromWinner].UpdateWeight(learningRate, degree, coordinate);
                distanceFromWinner++;
            }
        }

        // Constant learning rate seems to give just as good results with better performance.
        //        double learningRate { get { return 1.0 * s / lambda; } }
        double learningRate { get { return 0.1; } }
    }
}

