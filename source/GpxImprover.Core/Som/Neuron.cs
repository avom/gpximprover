﻿using System;
using Geo;
using Geo.Gps;

namespace GpxImprover.Core.Som
{
    public class Neuron
    {
        readonly Fix fix;

        public Neuron(Fix fix)
        {
            if (fix == null)
                throw new ArgumentNullException("fix");

            this.fix = fix;
            this.Coordinate = fix.Coordinate;
        }

        public Coordinate Coordinate { get ; private set; }

        public void UpdateWeight(double learningRate, double neighborhoodDegree, Coordinate x)
        {
            if (x == null)
                throw new ArgumentNullException("x");

            double learningFactor = learningRate * neighborhoodDegree;
            var wt = Coordinate;
            double latitude = wt.Latitude + learningFactor * (x.Latitude - wt.Latitude);
            double longitude = wt.Longitude + learningFactor * (x.Longitude - wt.Longitude);
            Coordinate = new Coordinate(latitude, longitude);
        }

        public void Map()
        {
            fix.Coordinate = Coordinate;
        }
    }
}