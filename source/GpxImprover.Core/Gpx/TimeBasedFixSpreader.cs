﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Gps;

namespace GpxImprover.Core.Gpx
{
    public class TimeBasedFixSpreader: IFixSpreader
    {
        public void Spread(List<Fix> fixes, int fromIndex, int toIndex)
        {
            if (fixes == null)
                throw new ArgumentNullException("fixes");
            if (fromIndex >= toIndex)
                throw new ArgumentException("fromIndex must be less than toIndex");
                
            long firstTicks = fixes[fromIndex].TimeUtc.Ticks;
            long lastTicks = fixes[toIndex].TimeUtc.Ticks;
            double totalTicks = lastTicks - firstTicks;

            Coordinate first = fixes[fromIndex].Coordinate;
            Coordinate last = fixes[toIndex].Coordinate;
            double dLatitude = last.Latitude - first.Latitude;
            double dLongitude = last.Longitude - first.Longitude;
            for (int i = 1; i < toIndex - fromIndex; i++)
            {
                long ticks = fixes[i + fromIndex].TimeUtc.Ticks;
                double time = (ticks - firstTicks) / totalTicks;
                Coordinate current = fixes[i + fromIndex].Coordinate;
                double latitude = time * dLatitude + current.Latitude;
                double longitude = time * dLongitude + current.Longitude;
                fixes[i + fromIndex].Coordinate = new Coordinate(latitude, longitude);
            }
        }
    }
}

