﻿using System;
using System.Collections.Generic;
using Geo.Gps;

namespace GpxImprover.Core.Gpx
{
    public class MissingFixesRestorer
    {
        readonly IFixSpreader fixSpreader;
        readonly int maxSecondsBetweenFixes;

        List<Fix> originalFixes;
        List<Fix> fixes;

        public MissingFixesRestorer(IFixSpreader fixSpreader, int maxSecondsBetweenFixes)
        {
            if (fixSpreader == null)
                throw new ArgumentNullException("fixSpreader");
            if (maxSecondsBetweenFixes < 1)
            {
                throw new ArgumentOutOfRangeException("maxSecondsBetweenFixes", 
                    "There must be at least one second between two fixes");
            }

            this.fixSpreader = fixSpreader;
            this.maxSecondsBetweenFixes = maxSecondsBetweenFixes;
        }

        public void Restore(List<Fix> fixes)
        {
            if (fixes == null)
                throw new ArgumentNullException("fixes");

            this.fixes = fixes;
            originalFixes = new List<Fix>(fixes);
            fixes.Clear();
            fixes.Add(originalFixes[0]);
            for (int i = 1; i < originalFixes.Count; i++)
            {
                double diff = (originalFixes[i].TimeUtc - originalFixes[i - 1].TimeUtc).TotalSeconds;
                if (diff > maxSecondsBetweenFixes)
                {
                    int newFixCount = (int)((diff - 0.1) / maxSecondsBetweenFixes);
                    duplicateLastFix(maxSecondsBetweenFixes, newFixCount);
                    fixes.Add(originalFixes[i]);
                    fixSpreader.Spread(fixes, fixes.Count - newFixCount - 2, fixes.Count - 1);
                }
                else
                    fixes.Add(originalFixes[i]);
            }
        }

        void duplicateLastFix(int secondsDelta, int newFixCount)
        {
            for (int i = 0; i < newFixCount; i++)
            {
                Fix last = fixes[fixes.Count - 1];
                double latitude = last.Coordinate.Latitude;
                double longitude = last.Coordinate.Longitude;
                DateTime time = last.TimeUtc.AddSeconds(secondsDelta);
                fixes.Add(new Fix(latitude, longitude, time));
            }
        }
    }
}
