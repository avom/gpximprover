using System;
using Geo.Gps;
using Geo;
using System.Collections.Generic;

namespace GpxImprover.Core.Gpx
{
    public class UniformFixSpreader : IFixSpreader
    {
        public void Spread(List<Fix> fixes, int fromIndex, int toIndex)
        {
            if (fixes == null)
                throw new ArgumentNullException("fixes");

            Coordinate first = fixes[fromIndex].Coordinate;
            Coordinate last = fixes[toIndex].Coordinate;
            int count = toIndex - fromIndex + 1;
            double dLatitude = (last.Latitude - first.Latitude) / (count - 1);
            double dLongitude = (last.Longitude - first.Longitude) / (count - 1);
            for (int i = 1; i < toIndex - fromIndex; i++)
            {
                Coordinate current = fixes[i + fromIndex].Coordinate;
                double latitude = i * dLatitude + current.Latitude;
                double longitude = i * dLongitude + current.Longitude;
                fixes[i + fromIndex].Coordinate = new Coordinate(latitude, longitude);
            }
        }
    }
}

