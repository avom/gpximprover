﻿using System;
using System.Collections.Generic;
using Geo;
using Geo.Gps;
using Geo.Measure;
using GpxImprover.Core.Common;

namespace GpxImprover.Core.Gpx
{
    public class ErroneousStopRemover
    {
        readonly IFixSpreader fixSpreader;
        readonly DistanceCalculator calculator;
        readonly Speed minMovementSpeed = new Speed(0.25);
        List<Fix> fixes;

        public ErroneousStopRemover(IFixSpreader fixSpreader, DistanceCalculator calculator)
        {
            if (fixSpreader == null)
                throw new ArgumentNullException("fixSpreader");
            if (calculator == null)
                throw new ArgumentNullException("calculator");

            this.fixSpreader = fixSpreader;
            this.calculator = calculator;
        }

        public void SpreadErroneousStops(List<Fix> fixes)
        {
            if (fixes == null)
                throw new ArgumentNullException("fixes");

            this.fixes = fixes;
            if (fixes.Count < 3)
                return;

            int fromIndex = -1;
            for (int i = 1; i < fixes.Count; i++)
            {
                Coordinate current = fixes[i].Coordinate;
                Coordinate previous = fixes[i - 1].Coordinate;
                if (current.Equals(previous))
                {
                    if (fromIndex < 0)
                        fromIndex = i - 1;
                }
                else if (fromIndex >= 0)
                {
                    Speed firstPostStopSegment = getAverageSpeed(i - 1, i);
                    if (firstPostStopSegment.SiValue > 0.5)
                        fixSpreader.Spread(fixes, fromIndex, i);
                    fromIndex = -1;
                }
            }

            if (fromIndex >= 0)
            {
                var fromCoordinate = fixes[fromIndex].Coordinate;
                var lastCoordinate = fixes[fixes.Count - 1].Coordinate;
                if (!fromCoordinate.Equals(lastCoordinate))
                    fixSpreader.Spread(fixes, fromIndex, fixes.Count - 1);
            }
        }

        Speed getAverageSpeed(int fromIndex, int toIndex)
        {
            var distance = new Distance();
            for (int i = fromIndex; i < toIndex; i++)
                distance += calculator.GetDistance(fixes[i].Coordinate, fixes[i + 1].Coordinate);
            TimeSpan duration = fixes[toIndex].TimeUtc - fixes[fromIndex].TimeUtc;
            return new Speed(distance.SiValue, duration);
        }
    }
}

