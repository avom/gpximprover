﻿using System;
using Geo.Gps;
using System.Collections.Generic;

namespace GpxImprover.Core.Gpx
{
    public interface IFixSpreader
    {
        void Spread(List<Fix> fixes, int fromIndex, int toIndex);
    }
}

