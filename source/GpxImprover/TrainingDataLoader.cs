﻿using System;
using System.IO;
using GpxImprover.Core.Som;
using GpxImprover.Core.Gpx;

namespace GpxImprover
{
    public class TrainingDataLoader
    {
        readonly GpxFileFactory gpxFileFactory;
        readonly SelfOrganizingMap som;
        readonly ErroneousStopRemover stopRemover;

        public TrainingDataLoader(GpxFileFactory gpxFileFactory, SelfOrganizingMap som, 
                                  ErroneousStopRemover stopRemover)
        {
            if (gpxFileFactory == null)
                throw new ArgumentNullException("gpxFileFactory");
            if (som == null)
                throw new ArgumentNullException("som");
            if (stopRemover == null)
                throw new ArgumentNullException("stopRemover");

            this.gpxFileFactory = gpxFileFactory;
            this.som = som;
            this.stopRemover = stopRemover;
        }

        public void Load(string directory)
        {
            if (directory == null)
                throw new ArgumentNullException("directory");
            if (String.IsNullOrWhiteSpace(directory))
                throw new ArgumentException("Directory cannot be whitespace", "directory");

            foreach (string fileName in Directory.EnumerateFiles(directory, "*.gpx"))
            {
                var file = gpxFileFactory.Create();
                file.Load(fileName).ReduceLongBreaks();
                stopRemover.SpreadErroneousStops(file.Fixes);
                //file.Save(fileName + ".gpx");
                som.AddTrainingData(file.Fixes.ConvertAll(fix => fix.Coordinate));
            }
        }
    }
}

