﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Castle.Windsor;
using Geo;
using Geo.Geodesy;
using GpxImprover.Core.Common;
using GpxImprover.Core.Gpx;
using GpxImprover.Core.Som;

namespace GpxImprover
{
    class MainClass
    {
        static IWindsorContainer container = WindsorSetup.Container;

        public static void Main(string[] args)
        {
            if (!checkArguments(args))
                return;

            var som = container.Resolve<SelfOrganizingMap>();
            var gpxFileFactory = container.Resolve<GpxFileFactory>();
            var distanceCalculator = container.Resolve<DistanceCalculator>();
            try
            {
                loadTrainingData(args[1], som, gpxFileFactory);

                var gpxFile = gpxFileFactory.Create().Load(args[0]).ReduceLongBreaks();

                var oldCoords = gpxFile.Fixes.ConvertAll(x => x.Coordinate);
                som.Learn(gpxFile.Fixes);
                som.Map();
                var newCoords = gpxFile.Fixes.ConvertAll(x => x.Coordinate);

                Console.WriteLine("Distance before: " + distanceCalculator.GetDistance(oldCoords));
                Console.WriteLine("Distance after : " + distanceCalculator.GetDistance(newCoords));

                printDifferences("diff.txt", oldCoords, newCoords);

                gpxFile.Save(args[0] + ".output.gpx");
            }
            finally
            {
                container.Release(som);
                container.Release(gpxFileFactory);
                container.Release(distanceCalculator);
            }
        }

        static bool checkArguments(string[] args)
        {
            if (args.Length == 2)
                return true;

            Console.WriteLine("USAGE: " + Process.GetCurrentProcess().MainModule.FileName +
                " <gpx file to optimize> <folder containing training gpx files>");
            return false;
        }

        static void loadTrainingData(string directory, SelfOrganizingMap som, GpxFileFactory factory)
        {
            var stopSpreader = container.Resolve<ErroneousStopRemover>();
            try
            {
                var loader = new TrainingDataLoader(factory, som, stopSpreader);
                loader.Load(directory);
            }
            finally
            {
                container.Release(stopSpreader);
            }
        }

        static void printDifferences(string fileName, List<Coordinate> oldCoords, 
                                     List<Coordinate> newCoords)
        {
            using (var writer = new StreamWriter(fileName))
            {
                var calculator = new DistanceCalculator(new SpheroidCalculator());
                for (int i = 0; i < oldCoords.Count; i++)
                    writer.WriteLine(i + "\t" + calculator.GetDistance(oldCoords[i], newCoords[i]));
            }
        }
    }
}
