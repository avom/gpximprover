﻿using System;
using GpxImprover.Core.Gpx;

namespace GpxImprover
{
    public class GpxFileFactory
    {
        readonly ErroneousStopRemover stopRemover;
        readonly MissingFixesRestorer fixesRestorer;

        public GpxFileFactory(ErroneousStopRemover stopRemover, MissingFixesRestorer fixesRestorer)
        {
            if (stopRemover == null)
                throw new ArgumentNullException("stopRemover");
            if (fixesRestorer == null)
                throw new ArgumentNullException("fixesRestorer");
            this.stopRemover = stopRemover;
            this.fixesRestorer = fixesRestorer;
        }

        public GpxFile Create()
        {
            return new GpxFile(stopRemover, fixesRestorer);
        }
    }
}

