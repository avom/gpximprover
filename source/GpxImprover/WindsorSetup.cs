﻿using Castle.Windsor;
using Castle.MicroKernel.Registration;
using GpxImprover.Core.Gpx;
using GpxImprover.Core.Common;
using Geo.Abstractions.Interfaces;
using Geo.Geodesy;
using GpxImprover.Core.Som;

namespace GpxImprover
{
    public static class WindsorSetup
    {
        public static IWindsorContainer Container { get; private set; }

        static WindsorSetup()
        {
            Container = new WindsorContainer();

            Container.Register(Component.For<IGeodeticCalculator>().ImplementedBy<SpheroidCalculator>());

            Container.Register(Component.For<DistanceCalculator>());

            Container.Register(Component.For<IFixSpreader>().ImplementedBy<TimeBasedFixSpreader>());
            Container.Register(Component.For<ErroneousStopRemover>());
            Container.Register(Component.For<MissingFixesRestorer>().
                DependsOn(
                    Parameter.ForKey("maxSecondsBetweenFixes").
                    Eq(Configuration.MaxSecondsBetweenFixes.ToString())
                )
            );
            Container.Register(Component.For<GpxFileFactory>());

            Container.Register(Component.For<INeighborhoodFunction>().
                ImplementedBy<GaussianNeighborhoodFunction>());
            Container.Register(Component.For<SelfOrganizingMap>().LifestyleTransient().
                DependsOn(
                    Parameter.ForKey("lambda").
                    Eq(Configuration.Lambda.ToString())
                )
            );

            Container.Register(Component.For<TrainingDataLoader>());
        }
    }
}

