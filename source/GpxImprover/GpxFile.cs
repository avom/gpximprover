﻿using System;
using System.Collections.Generic;
using System.IO;
using Geo.Gps;
using GpxImprover.Core.Gpx;

namespace GpxImprover
{
    public class GpxFile
    {
        readonly ErroneousStopRemover stopRemover;
        readonly MissingFixesRestorer missingFixesRestorer;

        GpsData gpx;

        public GpxFile(ErroneousStopRemover stopRemover, MissingFixesRestorer missingFixesRestorer)
        {
            this.stopRemover = stopRemover;
            this.missingFixesRestorer = missingFixesRestorer;
        }

        public List<Fix> Fixes
        { 
            get { return gpx.Tracks[0].Segments[0].Fixes; }
        }

        public GpxFile Load(string fileName)
        {
            if (fileName == null)
                throw new ArgumentNullException("fileName");
            if (String.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("File name cannot be white space", "fileName");

            using (var stream = new FileStream(fileName, FileMode.Open))
            {
                gpx = GpsData.Parse(stream);
            }
            return this;
        }

        public void Save(string fileName)
        {
            if (String.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("File name cannot be white space", "fileName");

            using (var writer = new StreamWriter(fileName))
            {
                writer.WriteLine(gpx.ToGpx());
            }
        }

        public GpxFile ReduceLongBreaks()
        {
            if (stopRemover != null)
                stopRemover.SpreadErroneousStops(Fixes);
            if (missingFixesRestorer != null)
                missingFixesRestorer.Restore(Fixes);
            return this;
        }
    }
}

