﻿using System;

namespace GpxImprover
{
    public static class Configuration
    {
        public const int MaxSecondsBetweenFixes = 3;

        public const int Lambda = 30;
    }
}

